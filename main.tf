data "aws_availability_zones" "az" {}

data "aws_ami" "latest_amazon_linux" {
  owners      = ["137112412989"]
  most_recent = true
  filter {
    name   = "name"
    values = ["al2023-ami-202*-kernel-6.1-x86_64"]
  }
}

resource "aws_default_vpc" "default" {}

resource "aws_default_subnet" "default_az1" {
  availability_zone = data.aws_availability_zones.az.names[0]
}

resource "aws_default_subnet" "default_az2" {
  availability_zone = data.aws_availability_zones.az.names[1]
}

resource "aws_security_group" "gitlab" {
  name   = "gitlab sg"
  vpc_id = aws_default_vpc.default.id
  dynamic "ingress" {
    for_each = var.open_ports_for_sg
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "gitlab SG"
  }
}

resource "aws_launch_template" "gitlab" {
  name                   = "gitlab-server-HA-LT"
  image_id               = data.aws_ami.latest_amazon_linux.id
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.gitlab.id]
  user_data              = base64encode(templatefile("${path.module}/user_data.sh.tpl", { public_dns = aws_lb.gitlab.dns_name }))
}

resource "aws_autoscaling_group" "gitlab" {
  name                = "gitlab-server-HA-ASG-Ver-${aws_launch_template.gitlab.latest_version}"
  min_size            = 2
  max_size            = 2
  min_elb_capacity    = 2
  health_check_type   = "ELB"
  vpc_zone_identifier = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]
  target_group_arns   = [aws_lb_target_group.gitlab.arn]

  launch_template {
    id      = aws_launch_template.gitlab.id
    version = aws_launch_template.gitlab.latest_version
  }
  dynamic "tag" {
    for_each = {
      Name    = "gitlab server in ASG-v${aws_launch_template.gitlab.latest_version}"
      project = "gitlab for luxoft"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb" "gitlab" {
  name               = "gitlab-server-HA-ALB"
  load_balancer_type = "application"
  security_groups    = [aws_security_group.gitlab.id]
  subnets            = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]
}

resource "aws_lb_target_group" "gitlab" {
  name                 = "gitlab-server-HA-TG"
  vpc_id               = aws_default_vpc.default.id
  port                 = 80
  protocol             = "HTTP"
  deregistration_delay = 10 # seconds
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.gitlab.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.gitlab.arn
  }
}
