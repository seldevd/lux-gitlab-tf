output "web_lb_url" {
  value = aws_lb.gitlab.dns_name
}
