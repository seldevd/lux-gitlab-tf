variable "region" {
  type        = string
  default     = "us-east-1"
}

variable "open_ports_for_sg" {
  description = "List of default ports"
  type        = list(any)
  default     = ["80", "443", "22"]
}

variable "common_project_tags" {
  type = map
  default = {
      Owner     = "Dmitrii Seldev"
      CreatedBy = "Terraform"
      Project   = "GitLab for Luxoft"
  }
}

variable "instance_type" {
  type        = string
  default     = "t3.xlarge"
}